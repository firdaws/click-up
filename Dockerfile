FROM node:12.18-alpine AS ui-build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
COPY expressConfig.js /usr/src/app/node_modules/ngf-server/expressConfig.js
RUN npm run build

#COPY my-app/ ./my-app/
#RUN cd my-app && npm install @angular/cli && npm install && npm run build

#FROM node:10 AS server-build
#WORKDIR /root/
#COPY --from=ui-build /usr/src/app/dist ./my-app/dist
#COPY package*.json ./
#RUN npm install
#COPY server.js .

EXPOSE 8808

WORKDIR /usr/src/app/node_modules/ngf-server
CMD node server.js
#docker run -d -p 8808:8808 --name clickup daws612/clickup:latest
