import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { EventsListComponent } from './events/events-list/events-list.component';

import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatIconModule} from '@angular/material/icon';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import { A11yModule } from '@angular/cdk/a11y';
import { CdkTableModule } from '@angular/cdk/table';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { EventService } from './events/event.service';
import {FlexLayoutModule} from "@angular/flex-layout";

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EventListFormatterPipe } from './events/events-list/event-list-formatter.pipe';
import { SearchComponent } from './events/events-list/search/search.component';

const appRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'events' },
  {path: 'events', component: EventsListComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    EventsListComponent,
    EventListFormatterPipe,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes
      //{ enableTracing: true } // <-- debugging purposes only
    ),
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    MatSortModule,
    MatIconModule,
    DragDropModule,
    CommonModule,
    A11yModule,
    CdkTableModule,
    CdkTableModule,
    MatIconModule,
    ScrollingModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,

    HttpClientModule
  ],
  providers: [
    EventService
  ],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
