import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IEvent } from './event.model';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class EventService {
  public events: IEvent[] = [];

  constructor(private http: HttpClient) {}

  getEvents(): Observable<IEvent[]> {
    return this.http
      .get<IEvent[]>('/api/events')
      .pipe(catchError(this.handleError<IEvent[]>([])));
  }

  filterListBySearch(searchTerm: string, displayedColumnNames: string[]): IEvent[] {
    if(!searchTerm)
      return this.events;
    let eventsList = this.events.filter((event: any) => {
      for(const columnName of displayedColumnNames) {
        if((event[columnName]).toString().toLocaleLowerCase().indexOf(searchTerm) > -1)
          return true;
      }
      return false;

    });
    return eventsList;
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
