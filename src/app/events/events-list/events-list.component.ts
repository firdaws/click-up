import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IEvent } from '../event.model';
import { EventService } from '../event.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss'],
})
export class EventsListComponent implements OnInit, AfterViewInit {
  dataSource: any;
  displayedColumns = ['name', 'date', 'time', 'price'];
  columns: any[] = [
    { name: 'name', title: 'Name', dataType: 'string' },
    { name: 'date', title: 'Date', dataType: 'date' },
    { name: 'time', title: 'Time' },
    { name: 'price', title: 'Price', dataType: 'currency' },
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  sortControl = new FormControl();

  constructor(private eventService: EventService) {}

  ngOnInit() {
    this.eventService.getEvents().subscribe((events) => {
      this.eventService.events = events;
      this.dataSource.data = events;
    });
    this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  tableDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.displayedColumns,
      event.previousIndex,
      event.currentIndex
    );
  }

  sortEvents(sortOption: string) {
    if (this.dataSource.data) {
      this.dataSource.data = this.dataSource.data.sort(this.dynamicSort(sortOption));
    }
  }

  dynamicSort(property: string = 'name') {
    var sortOrder = 1;
    // if(property[0] === "-") {
    //     sortOrder = -1;
    //     property = property.substr(1);
    // }
    return function (a: any, b: any) {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      var result = 0;
      var valA = a[property];
      var valB = b[property];
      if (property === 'date') {
        valA = new Date(a[property]);
        valB = new Date(b[property]);
      } else if (property === 'time') {
        var timeA = new Date('1/1/2013 ' + a[property]);
        valA = timeA.getHours() + timeA.getMinutes();
        var timeB = new Date('1/1/2013 ' + b[property]);
        valB = timeB.getHours() + timeB.getMinutes();
      }
      if(typeof(valA) === "string") {
        valA = valA.toLocaleLowerCase();
        valB = valB.toLocaleLowerCase();
      }

      result = valA < valB ? -1 : valA > valB ? 1 : 0;

      return result * sortOrder;
    };
  }

  updateTable(event: any) {
    this.dataSource.data = event;
    this.sortControl.reset();
  }
}
