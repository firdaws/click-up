import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IEvent } from '../../event.model';
import { EventService } from '../../event.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  searchTerm: string = '';
  searchControlSub: Subscription = new Subscription();
  searchControl = new FormControl();

  @Output() searchResultsEmitted = new EventEmitter<IEvent[]>();
  @Input() displayedColumns: string[] = [];

  constructor(private eventService: EventService) {}

  ngOnInit() {
    // debounce keystroke events
    this.searchControlSub = this.searchControl.valueChanges
      .pipe(debounceTime(800), distinctUntilChanged())
      .subscribe((newValue: string) => {
        this.searchTerm = newValue;
        this.search();
      });
  }

  ngOnDestroy() {
    this.searchControlSub.unsubscribe();
  }

  search() {
    const filteredResults: IEvent[] = this.eventService.filterListBySearch(
      this.searchTerm,
      this.displayedColumns
    );
    this.searchResultsEmitted.emit(filteredResults);
  }
}
