import { CurrencyPipe, DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'eventListFormatter'
})
export class EventListFormatterPipe extends DatePipe implements PipeTransform {

  transform(value: any, dataType?: string): any {
    if(dataType === "string")
      value = this.titleCase(value);
    if(dataType === "date")
      value = super.transform(value, "dd MMM yyyy");
    if(dataType === "currency")
      value = '$ ' + value;

    return value;
  }

  titleCase(str: string) {
    return str.toLowerCase().split(' ').map(function(word) {
      return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
  }

}
